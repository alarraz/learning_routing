import subprocess
import numpy as np
import matplotlib.pyplot as plt

#ip de Eduard
ip_dst = "10.200.211.203"
seconds = 2
num_bucles = 5

dades = []

username = "admin"
password = "Kirchoff"
port = 22
hostname = "10.200.211.203"
command = "ls"

import paramiko
client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
client.connect(hostname, port=port, username=username, password=password)
stdin, stdout, stderr = client.exec_command(command)

s=stdout.read()

s.decode().split()


for i in range(num_bucles):

	cmd = "iperf -c {} -t {} -y C".format(ip_dst,seconds)
	out = subprocess.check_output(cmd.split())
	vel = round(int(out.strip().split(',')[-1])/1000000.0, 2)
	dades.append(vel)
	print(vel)



n_groups = num_bucles

#means_men = (20, 35, 30, 35, 27)
means_men = dades
std_men = (2, 3, 4, 1, 2)

# means_women = (25, 32, 34, 20, 25)
# std_women = (3, 5, 2, 3, 3)

fig, ax = plt.subplots()

index = np.arange(n_groups)
bar_width = 0.35

opacity = 0.4
error_config = {'ecolor': '0.3'}

rects1 = plt.bar(index, means_men, bar_width,
                 alpha=opacity,
                 color='b',
                 yerr=std_men,
                 error_kw=error_config,
                 label='Men')

#rects2 = plt.bar(index + bar_width, means_women, bar_width,
                 #alpha=opacity,
                 #color='r',
                 #yerr=std_women,
                 #error_kw=error_config,
                 #label='Women')

plt.xlabel('Group')
plt.ylabel('Scores')
plt.title('Test de velocidad del radioenlace')
plt.xticks(index + bar_width, [str(v) for v in dades])
plt.legend()

plt.tight_layout()
plt.show()
