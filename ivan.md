## Ivan Rallo
# Configuracion inicial

	Definir nombre de interfaz
	
		[admin@MikroTik]>/interface ethernet set [ find default-name=ether1 ] name=eth1 master-port=none
		[admin@MikroTik]>set [ find default-name=ether2 ] name=eth2 master-port=none
		[admin@MikroTik]>set [ find default-name=ether3 ] name=eth3 master-port=none
		[admin@MikroTik]>set [ find default-name=ether4 ] name=eth4 master-port=none
		[admin@MikroTik]>set [ find default-name=ether5 ] name=eth5 master-port=none
	
		[admin@MikroTik]>/ip address set 0 interface=eth2  
		
	Quitar el bridge
	
		[admin@MikroTik]>/interface bridge remove
		
	Quitar dhcp
	
		[admin@MikroTik]>/interface>/ip dhcp-server print
		[admin@MikroTik]>/interface>/ip dhcp-server remove 0
		
	Cambiar el nombre de identidad
	
		[admin@MikroTik]>/interface> /system identity set name=mkt2
		
	Desactivar wireless
	
		[admin@mkt2]>/interface wireless disable 0
		
	Eliminar nat y desactivar el filtro en el 3
	
		[admin@mkt2]>/ip firewall nat remove 0
		[admin@mkt2]>/ip firewall filter disable 3
