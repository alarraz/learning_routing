# Configuración de routers mikrotik para radioenlace

## Configuración inicial

Si hemos de resetear el router hacemos:

	/system reset-configuration  no-defaults=yes

Primero ponemos una ip por defecto

	/ip address add address=192.168.88.1/24 interface=ether18
	
Creamos el puente entre interfaces wlan y ether12

	/interface bridge add name=puente
	/interface bridge port
	add bridge=puente interface=ether12
	add bridge=puente interface=wlan1
	
El radioenlace usará la red 172.16.88.0/24 y nuestro router será el .1:

	/ip address add address=172.16.88.1/24 interface=puente
	
