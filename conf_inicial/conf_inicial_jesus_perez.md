/interface ethernet
set [ find default-name=ether1 ] name=ether1-gateway
set [ find default-name=ether2 ] name=ether2-master-local
set [ find default-name=ether3 ] master-port=ether2-master-local name=\
    ether3-slave-local
set [ find default-name=ether4 ] master-port=ether2-master-local name=\
    ether4-slave-local
set [ find default-name=ether5 ] master-port=ether2-master-local name=\
    ether5-slave-local



[admin@MikroTik] > 
[admin@MikroTik] > interface ethernet set [ find default-name=ether1 ] name=eth1 master-port=none
[admin@MikroTik] > interface ethernet set [ find default-name=ether2 ] name=eth2 master-port=none   
[admin@MikroTik] > interface ethernet set [ find default-name=ether3 ] name=eth3 master-port=none  
[admin@MikroTik] > interface ethernet set [ find default-name=ether4 ] name=eth4 master-port=none  
[admin@MikroTik] > interface ethernet set [ find default-name=ether5 ] name=eth5 master-port=none  



/interface ethernet
set [ find default-name=ether1 ] name=eth1
set [ find default-name=ether2 ] name=eth2
set [ find default-name=ether3 ] name=eth3
set [ find default-name=ether4 ] name=eth4
set [ find default-name=ether5 ] name=eth5




/interface wireless security-profiles
set [ find default=yes ] authentication-types=wpa-psk,wpa2-psk mode=\
    dynamic-keys wpa-pre-shared-key=46ED02934E92 wpa2-pre-shared-key=\
    46ED02934E92


[admin@mkt1] > ip address set 0 interface= ether2 
/interface bridge port
add bridge=bridge-local interface=eth2
add bridge=bridge-local interface=wlan1


/interface> /ip dhcp-server remove 0 


[admin@MikroTik] /interface> /system identity set name=mkt1
[admin@mkt1] /interface>   

[admin@mkt1] /interface wireless> remove

[admin@mkt1] > /ip firewall nat remove 0

[admin@mkt1] > /ip firewall filter disable
numbers: 3
[admin@mkt1] > /ip firewall filter disable
numbers: 6
