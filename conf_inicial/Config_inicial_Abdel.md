# Cambiar nombre interfaces/quitar masterport

/interface ethernet> set [ find default-name=ether1 ] name=eth1
/interface ethernet> set [ find default-name=ether2 ] name=eth2
/interface ethernet> set [ find default-name=ether3 ] master-port=none name=eth3
/interface ethernet> set [ find default-name=ether4 ] master-port=none name=eth4
/interface ethernet> set [ find default-name=ether5 ] master-port=none name=eth5


# Cambiar nombre router
/system identity> set name=mkt10 
name: mkt10


# Cambiar bridge y quitar el otro

/ip address set 0 interface=eth2

# Quitar dhcp server

/ip dhcp-server remove 0
