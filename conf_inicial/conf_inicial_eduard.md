##Configuracio inicial Router
#Eduard Caballol

#Apagar NetworkManager
systemctl stop NetworkManager

#Asignar ip Targeta de red
ip a a 192.168.88.2/24 dev enp0s29u1u2

#Entrar en mikrotik
Telnet 192.168.88.1
login: admin
pasword: ---

# Resetear configuracion router desde "telnet 192.168.88.1"
/system reset-configuration

# Mostrar interfaces
/interface print

#Entrar en interface ethernet
/interface ethernet

# Cambiar nombre de las interfaces
set [ find default-name=ether1 ] name=eth1
set [ find default-name=ether2 ] name=eth2
set [ find default-name=ether3 ] name=eth3
set [ find default-name=ether4 ] name=eth4
set [ find default-name=ether5 ] name=eth5

#Quitar MasterPort:
set [ find default-name=ether2 ] master-port=none                           
set [ find default-name=ether4 ] master-port=none 
set [ find default-name=ether5 ] master-port=none 
set [ find default-name=ether1 ] master-port=none 

#Asignamos ip al eth2
/ip address set 0 interface=eth2

#Quitamos el puerto "bridge" 
/interface bridge remove 0

#Quitamos el dhcp-server
/ip dhcp-server remove 0

