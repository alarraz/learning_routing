# Configuración inicial

Cambiar nombre router:

	/system identity 
	/system identity set name=mkt07
	
Cambiar nombre interfaces:

	/interface ethernet set [ find default-name=ether1 ] name=eth1
	/interface ethernet set [ find default-name=ether2 ] name=eth2
	/interface ethernet set [ find default-name=ether3 ] name=eth3
	/interface ethernet set [ find default-name=ether4 ] name=eth4
	/interface ethernet set [ find default-name=ether5 ] name=eth5
	
Quitar MasterPort:

	/interface ethernet set [ find default-name=ether2 ] master-port=none                           
	/interface ethernet set [ find default-name=ether3 ] master-port=none   
	/interface ethernet set [ find default-name=ether4 ] master-port=none 
	/interface ethernet set [ find default-name=ether5 ] master-port=none 
	/interface ethernet set [ find default-name=ether1 ] master-port=none 

Cambiar interface:

	/ip address print
		Flags: X - disabled, I - invalid, D - dynamic 
		 #   ADDRESS            NETWORK         INTERFACE                              
		 0   ;;; default configuration
			 192.168.88.1/24    192.168.88.0    bridge-local                           
	/ip address set 0 interface=eth2
	/ip address print
		Flags: X - disabled, I - invalid, D - dynamic 
		 #   ADDRESS            NETWORK         INTERFACE                              
		 0   ;;; default configuration
			 192.168.88.1/24    192.168.88.0    eth2        

Quitar Bridge:

	/interface bridge print
		Flags: X - disabled, R - running 
		 0  R name="bridge-local" mtu=1500 l2mtu=1598 arp=enabled 
			  mac-address=D4:CA:6D:B4:65:D5 protocol-mode=rstp priority=0x8000 
			  auto-mac=no admin-mac=D4:CA:6D:B4:65:D5 max-message-age=20s 
			  forward-delay=15s transmit-hold-count=6 ageing-time=5m 
	/interface bridge remove 0
	/interface bridge print
		Flags: X - disabled, R - running 

Quitar dhcp-server:

	/ip dhcp-server 
	/ip dhcp-server print
		Flags: X - disabled, I - invalid 
		 #   NAME     INTERFACE     RELAY           ADDRESS-POOL     LEASE-TIME ADD-ARP
		 0 I default  (unknown)                     default-dhcp     3d        
	/ip dhcp-server remove 0
	/ip dhcp-server print
		Flags: X - disabled, I - invalid 
		#   NAME     INTERFACE     RELAY           ADDRESS-POOL     LEASE-TIME ADD-ARP

Todas las instrucciones en un copy-paste:

```
/system identity set name=mkt07
/interface ethernet set [ find default-name=ether1 ] name=eth1 master-port=none
/interface ethernet set [ find default-name=ether2 ] name=eth2 master-port=none
/interface ethernet set [ find default-name=ether3 ] name=eth3 master-port=none
/interface ethernet set [ find default-name=ether4 ] name=eth4 master-port=none
/interface ethernet set [ find default-name=ether5 ] name=eth5 master-port=none
/ip address set 0 interface=eth2
/interface bridge remove 0
/ip dhcp-server remove 0
```