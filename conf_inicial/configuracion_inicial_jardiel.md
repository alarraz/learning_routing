Cambiar nombre y quitar master port

/interface ethernet
set [ find default-name=ether1 ] name=eth1 master-port=none
set [ find default-name=ether2 ] name=eth2 master-port=none
set [ find default-name=ether3 ] name=eth3 master-port=none
set [ find default-name=ether4 ] name=eth4 master-port=none
set [ find default-name=ether5 ] name=eth5 master-port=none

Cambiar nombre Router

/system identity set name=Mkt14


Cambiar bridge

/ip address set 0 interface=eth2
/ip bridge remove 0

Quitar dhcp server

/ip dhcp-server remove 0
