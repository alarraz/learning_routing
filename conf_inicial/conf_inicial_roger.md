# Configuración inicial

Apagar Network Manager

    systemctl stop NetworkManager

Asignar ip al puerto usb

    ip a a 192.168.88.204 dev enp0s29u1u2

Resetear configuracion router desde "telnet 192.168.88.1"

    /system reset-configuration

Mostrar interfaces

    /interface print

Cambiar nombre de las interfaces

    /interface set name=eth1 0
    /interface set name=eth2 1
    /interface set name=eth3 2
    /interface set name=eth4 3
    /interface set name=eth5 4

Asignamos ip al eth2

    /ip address set 0 interface=eth2

Quitamos el puerto "bridge"   !!antes asignar ip!!
    /interface bridge remove 0

Quitamos los masterport de los puertos
    /interface ethernet set [ find default-name=ether3 ] master-port=none name=eth3
    /interface ethernet set [ find default-name=ether4 ] master-port=none name=eth4
    /interface ethernet set [ find default-name=ether5 ] master-port=none name=eth5

Quitamos el dhcp-server

    /ip dhcp-server remove 0
    
Asignamos gateway del router

	/ip route add gateway=10.200.212.1 dst-address=0.0.0.0/0
