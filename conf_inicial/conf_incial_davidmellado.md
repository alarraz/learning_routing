#Cambiar nombre interficies

/interface ethernet
set [ find default-name=ether1 ] name=eth01  
set [ find default-name=ether2 ] name=eth2   
set [ find default-name=ether3 ] name=eth3   
set [ find default-name=ether4 ] name=eth4   
set [ find default-name=ether5 ] name=eth5   

#Cambiar nombre interficie

/interface identity> name=mkt10

#Quitamos el master-port

set [ find default-name=ether1 ] name=eth01  master-port=none
set [ find default-name=ether2 ] name=eth2   master-port=none
set [ find default-name=ether3 ] name=eth3   master-port=none
set [ find default-name=ether4 ] name=eth4   master-port=none
set [ find default-name=ether5 ] name=eth5   master-port=none

#Para quitar el bridge

/interface bridge> remove0

#Para introducir bridge

/interface bridge
add admin-mac=D4:CA:6D:A4:E6:BB auto-mac=no l2mtu=1598 name=bridge-local \
    protocol-mode=rstp
/interface bridge port
add bridge=bridge-local interface=eth2
add bridge=bridge-local interface=wlan1


#Para quitar el dhcp-server

/dhcp-server> remove0

#Para introducir el dhcp-server

/ip dhcp-server network
add address=192.168.88.0/24 comment="default configuration" dns-server=\
    192.168.88.1 gateway=192.168.88.1




