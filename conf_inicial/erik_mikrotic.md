# cambiar los nombres de las interfaces y sacarle el puente

	[admin@MikroTik] > /interface ethernet set [ find default-name=ether2 ] name=eth2 master-port=none
	[admin@MikroTik] > /interface ethernet set [ find default-name=ether3 ] name=eth3 master-port=none  
	[admin@MikroTik] > /interface ethernet set [ find default-name=ether4 ] name=eth4 master-port=none   
	[admin@MikroTik] > /interface ethernet set [ find default-name=ether5 ] name=eth5 master-port=none


# eliminar el dhcp server

	 /ip dhcp-server remove 0    
	 /ip dhcp-server network remove 0


#Cambiar nombre del router

	> system identity set name=mkt6

#Desactivar wifi

	> interface wireless disable 0
 
#Cambiar el bridge de interfaz

	ip address set 0 interface=eth2

#Eliminar el bridge

	interface bridge port export   
	interface bridge remove 0

#deshabilitar firewall3 y eliminar NAT 

	 ip firewall filter disable 3
	 ip firewall nat remove 0   
