﻿# Configuración inicial

```
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=\
    20/40mhz-ht-above distance=indoors l2mtu=2290 mode=ap-bridge ssid=\
    MikroTik-2FDE01

/interface ethernet
set [ find default-name=ether1 ] name=eth1 master-port=none
set [ find default-name=ether2 ] name=eth2 master-port=none
set [ find default-name=ether3 ] name=eth3 master-port=none
set [ find default-name=ether4 ] name=eth4 master-port=none
set [ find default-name=ether5 ] name=eth5 master-port=none
 

/ip neighbor discovery
set eth1 discover=no

/interface wireless security-profiles
set [ find default=yes ] authentication-types=wpa-psk,wpa2-psk group-ciphers=\
    tkip,aes-ccm mode=dynamic-keys supplicant-identity=MikroTik \
    unicast-ciphers=tkip,aes-ccm wpa-pre-shared-key=3BD2024BA288 \
    wpa2-pre-shared-key=3BD2024BA288

/ip hotspot user profile
set [ find default=yes ] idle-timeout=none keepalive-timeout=2m \
    mac-cookie-timeout=3d

/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254

/port
set 0 name=serial0

/tool user-manager customer
add backup-allowed=yes disabled=no login=admin password="" \
    paypal-accept-pending=no paypal-allowed=no paypal-secure-response=no \
    permissions=owner signup-allowed=no time-zone=-00:00

/ip address
add address=192.168.88.1/24 comment="default configuration" interface=eth2 \
    network=192.168.88.0

/ip dhcp-client
add comment="default configuration" dhcp-options=hostname,clientid disabled=\
    no interface=eth1

/ip dns
set allow-remote-requests=yes

/ip dns static
add address=192.168.88.1 name=router

/ip firewall filter
add chain=input comment="default configuration" protocol=icmp
add chain=input comment="default configuration" connection-state=established
add chain=input comment="default configuration" connection-state=related
add action=drop chain=input comment="default configuration" disabled=yes \
    in-interface=eth1
add chain=forward comment="default configuration" connection-state=\
    established
add chain=forward comment="default configuration" connection-state=related
add action=drop chain=forward comment="default configuration" \
    connection-state=invalid

/system identity
set name=MKT15

/system lcd
set contrast=0 enabled=no port=parallel type=24x4

/system lcd page
set time disabled=yes display-time=5s
set resources disabled=yes display-time=5s
set uptime disabled=yes display-time=5s
set packets disabled=yes display-time=5s
set bits disabled=yes display-time=5s
set version disabled=yes display-time=5s
set identity disabled=yes display-time=5s
set wlan1 disabled=yes display-time=5s
set eth1 disabled=yes display-time=5s
set eth2 disabled=yes display-time=5s
set eth3 disabled=yes display-time=5s
set eth4 disabled=yes display-time=5s
set eth5 disabled=yes display-time=5s

/system leds
set 0 interface=wlan1

/system routerboard settings
set cpu-frequency=360MHz

/tool mac-server
set [ find default=yes ] disabled=yes
add interface=eth2
add interface=eth3
add interface=eth4
add interface=eth5
add interface=wlan1

/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes
add interface=eth2
add interface=eth3
add interface=eth4
add interface=eth5
add interface=wlan1
```
