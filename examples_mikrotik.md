Asignamos las direcciones IP a las interfícies.

/ip address 
	add address=192.168.88.1/24 comment="default configuration" disabled=no \interface=eth2 \
	network=192.168.88.0
	add address=10.200.212.101/24 interface=eth5 network=10.200.212.0
	add address=172.17.101.1/24 disabled=no interface=eth3 network=\172.17.101.0
	add address=172.16.101.1/24 interface=bridge1 network=172.16.101.0

/ip firewall nat
add action=masquerade chain=srcnat disabled=no out-interface=eth5

/ip pool
add name=dhcp_pool1 ranges=172.16.109.100-172.16.109.254

/ip dhcp-server network
add address=172.16.101.0/24 dns-server=8.8.8.8 gateway=172.16.101.1

/ip dhcp-server
add address-pool=dhcp_pool1 disabled=no interface=bridge1 name=dhcp1

/ip route add distance=1 gateway=10.200.212.2 routing-mark=adsl
/ip route add distance=1 gateway=10.200.212.1 routing-mark=escola

/ip firewall mangle
add action=mark-routing chain=prerouting in-interface=bridge1 new-routing-mark=adsl src-address=172.16.104.0/24
add action=mark-routing chain=prerouting in-interface=eth3 new-routing-mark=escola src-address=172.17.104.0/24


#cambiar dns por el de la escuela, aseguraros que los servidores dhcp dan el 10.1.1.200
/ip dhcp-server network
add address=172.16.104.0/24 dns-server=10.1.1.200 gateway=172.16.104.1
add address=172.17.104.0/24 dns-server=10.1.1.200 gateway=172.17.104.1

#preparamos rutas con marcas
/ip route
add distance=1 gateway=10.200.212.2 routing-mark=adsl
add distance=1 dst-address=10.0.0.0/8 gateway=10.200.212.1 routing-mark=adsl
add distance=1 gateway=10.200.212.1 routing-mark=escola
add distance=1 gateway=10.200.212.1

/ip firewall mangle
add action=mark-routing chain=prerouting in-interface=bridge1 new-routing-mark=adsl src-address=\
    172.16.104.0/24
add action=mark-routing chain=prerouting in-interface=eth3 new-routing-mark=escola src-address=\
    172.17.104.0/24
add action=mark-packet chain=prerouting new-packet-mark=mirror src-address=10.1.1.188

/queue simple
add max-limit=128k/128k name=limit_down_wifi target=bridge1
/queue tree
add limit-at=100k max-limit=100k name=mirror_down packet-mark=mirror parent=global queue=default



