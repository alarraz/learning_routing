
/interface ethernet
set [ find default-name=ether1 ] name=eth01  master-port=none
set [ find default-name=ether2 ] name=eth2   master-port=none
set [ find default-name=ether3 ] name=eth3   master-port=none
set [ find default-name=ether4 ] name=eth4   master-port=none
set [ find default-name=ether5 ] name=eth5   master-port=none
/interface wireless security-profiles
set [ find default=yes ] authentication-types=wpa-psk,wpa2-psk group-ciphers=\
    tkip,aes-ccm mode=dynamic-keys supplicant-identity=MikroTik \
    unicast-ciphers=tkip,aes-ccm wpa-pre-shared-key=46ED02B4A4AF \
    wpa2-pre-shared-key=46ED02B4A4AF
